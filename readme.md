# Image Classification with Neural Network. 
## Work in progress.

dataset comes from here:
https://www.kaggle.com/puneet6060/intel-image-classification

The kernel supplied 15k images, that are pictures of mountains, sea, forest, street, glacier or buildings. 
The task is to train a model that will correctly identify the correct label for a new picture.

We're using a Sequential Neural Network (NN) from TensorFlow.Keras

<br>

#### Image examples:
![Image not found!](classification_image_examples.png "Image Examples")

Let's talk about Neural Networks. They're much more sophisticated than a lot of other models because of how they're composed of multiple layers. Each layer is very customisable, and layer A restructures the data for layer B etc etc. Because of this they can perform much more advanced data science than your standard regression, but they take a <strong>LOT</strong> longer to train.

To do a task like this we really need a NN, because a regular model can't handle this issue of 'oh these pixels are showing the features for buildings. They're in different places from other building pictures, but they're similarly placed relative to one-another'. The network will identify features from images, then features from these features etc.

Specifically, this is a <i>convolutional</i> NN, because the wrangling of passing our filter (of matrix weights) over the input and computing the output matrix is called convolution.

Here we set up how the model learns
```python
model = keras.models.Sequential() # a sequential model is the simplest neural network

# scans a 150x150x3 tensor (the image) for features, the 'scanning window' is 3x3 pixels
model.add(keras.layers.Conv2D(16, kernel_size = (3,3), input_shape = (150, 150, 3), activation = 'relu'))
model.add(keras.layers.Dropout(0.20))
model.add(keras.layers.MaxPooling2D(pool_size = (2,2), strides = 2))

model.add(keras.layers.Conv2D(32, kernel_size = (3,3), activation = 'relu'))
model.add(keras.layers.Dropout(0.20))
model.add(keras.layers.MaxPooling2D(pool_size = (2,2), strides = 2))

model.add(keras.layers.Conv2D(64, kernel_size = (3,3), activation = 'relu'))
model.add(keras.layers.Dropout(0.20))
model.add(keras.layers.MaxPooling2D(pool_size = (2,2), strides = 2))

model.add(keras.layers.Conv2D(128, kernel_size = (3,3), activation = 'relu'))
model.add(keras.layers.Dropout(0.20))
model.add(keras.layers.MaxPooling2D(pool_size = (2,2), strides = 2))

# the layer above returns a tensor. We need a single layer matrix so that we can make predictions
model.add(keras.layers.Flatten())
model.add(keras.layers.Dropout(0.50))

# make actual predictions
model.add(keras.layers.Dense(6, activation = 'softmax'))

# # configure how the model learns
model.compile(optimizer = keras.optimizers.Adam(lr=0.001), # Adam is a form of SGD that includes momentum
              loss = 'sparse_categorical_crossentropy', # 3+ possibility outcome classification
              metrics = ['accuracy'])
```

#### Activators
Let's talk about activators. In a node the elementwise matrix multiplication between inputs and weights is computed. The resulting number is often then converted to something useful by an activator (which is just a function/formula). There are three very common activators, one of which is suitable here...

Activator   |   returns |   Notes    |
------- | ----------------- | ------------- | 
Sigmoid  |      0 - 1     |    For shallow networks | 
Hyperbolic Tangent  |      -1 - 1     |    For shallow networks | 
ReLU (Rectified Linear Unit) | Converts <= 0 to 0 | For deep networks | 

We need ReLU, because we're going to be adding a fair few layers to this network. Sigmoid and Hyperbolic Tangent don't work so well because they compress all outputted values to between 0/-1 and 1, so ensuing layers of the network don't have as much variation to work with.

#### Dropout
You'll notice that the code above is littered with something called Dropout(). Dropout is a way of reducing overfitting. Neural Networks are extremely prone to overfitting, so you need to combat that. Dropout() works by disabling a proportion of the neurones in the previous layer, so that the model can't become over-reliant on it. 

Important note! No neurones are disabled during validation or testing, so you may well see performance initially much better during validation than training.

#### Training
And now we train it! A NN can handle its own cross validation - you don't need to encode it as manually as you do in sklearn.
```python
# early stopping - fitting iterations stop if we don't see continued improvement in the validation dataset
es = keras.callbacks.EarlyStopping(min_delta = 0.01, patience = 10, restore_best_weights = True)
training = model.fit(x = reshaped_images,
                     y = labels_nums,
                     verbose = 1,
                     validation_split = 0.15,
                     epochs = 50,
                     callbacks = [es])
```

Notice the bit about early stopping? We're doing 50 epochs (iterations) which is a lot. EarlyStopping will halt training if it doesn't see <i>min_delta</i> improvement in the validation dataset's loss (which is the defaul) in <i>patience</i> iterations.

#### Performance
Ok how did we do? Remember there are 6 classes (and they're equally-sized, by the way) so our correctness-baseline is 16.67% (if we just guessed randomly).

![Image not found!](model_accuracy.png "Model Performance")

Ok that's looking good! At its best the model is correct 80% of the time which is excellent! Let's have a look at what happens - see if we can see what happens when it gets it wrong.

![Image not found!](predictions.png "Predictions")

Ok so it got it wrong twice here (which is more or less in like with our 80% accuracy). Correct identifications are framed in green, incorrect are red. Looking at those two, I could easily have got them wrong myself - apparently they're 'glacier' and 'street' respectively. I think it would take a lot more work to get very slight increases in performance at this stage, it's performing about as well as a human (me) does.
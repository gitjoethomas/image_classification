import os
import cv2
from sklearn.utils import shuffle

def import_data(folder):
        
    """
    reads in all images for files in either datafiles/train/ or datafiles/test/, and applies the correct label for the 
    relevant folder. Returns shuffled images and labels.
    """
    
    # we'll append into these
    image_list = []
    label_list = []
    
    for image_category in os.listdir(f'datafiles/{folder}'):
        
        for image in os.listdir(f'datafiles/{folder}/{image_category}'):
            
            img = cv2.imread(f'datafiles/{folder}/{image_category}/{image}')
            img = cv2.resize(img, (150, 150)) # resizing is important because our images have to be identical apart from their content
            
            image_list.append(img)
            label_list.append(image_category)
                        
    return shuffle(image_list, label_list)